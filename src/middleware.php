<?php

use RedBeanPHP\Facade as R;
use Modules\HTTPCodesInterface;

/**
 * Function to compress the output buffer with gzip
 * @param  string $req       request
 * @param  string $res       response
 * @param  string $next      url to be redirect
 * @return array  $response  response
 */
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    ob_start('ob_gzhandler');
    return $response;
});



/**
 * Function that validate username in the database
 * the username is obtained in the payload of
 * the JWT that come in the request
 * @param  string $req       request
 * @param  string $res       response
 * @param  string $next      url to be redirect
 * @return array  $response  response
 */
$app->add(function ($req, $res, $next) use ($container) {
    $jwtParameters = array_key_exists('JWT', $container->get('parameters')) ? $container->get('parameters')['JWT'] : [];
    if (array_key_exists('whitelist', $jwtParameters)) {
        if (!is_null($req->getAttribute("route"))) {
            if (array_search($req->getAttribute("route")->getPattern(), $jwtParameters['whitelist'])!==false) {
                return $next($req, $res);
            }
        } elseif (array_search($req->getUri()->getPath(), $jwtParameters['whitelist'])!==false) {
            return $next($req, $res);
        }
    }

    $jwt = str_replace("Bearer ", "", $req->getHeaders()['HTTP_AUTHORIZATION'][0]);
    $container['jwt'] = $jwt;
    $payload = explode('.', $jwt, 3);
    $payload = base64_decode($payload[1]);
    $payload = json_decode($payload, true);

    $userTable = array_key_exists('table', $jwtParameters) ? $jwtParameters['table'] : 'user';
    $userPayloadField = array_key_exists('payload_user_field', $jwtParameters) ? $jwtParameters['payload_user_field'] : 'username';
    $userTableField = array_key_exists('table_user_field', $jwtParameters) ? $jwtParameters['table_user_field'] : 'user';
    R::selectDatabase($jwtParameters['connection']);
    $user = R::findOne($userTable, "{$userTableField} = ?", [$payload[$userPayloadField]]);

    if (empty($user)) {
        return UserNotFound($req, $res);
    } else {
        $container['user_claim'] = $user;
        $response = $next($req, $res);
        return $response;
    }
});

/**
 * Middleware for JWT
 */
$app->add(new Tuupola\Middleware\JwtAuthentication([
        "path" => "/api",
        "attribute" => "token",
        "ignore" => $parameters['JWT']['whitelist'],
        "algorithm" => [$parameters['JWT']['algorithm']],
        "secret" => $parameters['JWT']['secret'],
        "error" => function ($response, $arguments) {
            $data["status"] = "error";
            $data["message"] = $arguments;
            return $response->withJson($data, HTTPCodesInterface::HTTP_UNAUTHORIZED);
        },
        "secure" => false
    ]));

/**
 * Function to set the header in the output buffer
 * @param  string $req       request
 * @param  string $res       response
 * @param  string $next      url to be redirect
 * @return array  $response  response
 */
$app->add(new Tuupola\Middleware\CorsMiddleware([
    "origin" => ["*"],
    "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
    "headers.allow" => ["content-type","x-requested-with","authorization"],
    "headers.expose" => ["access-control-allow-origin"],
    "credentials" => false,
    "cache" => 0,
]));
