<?php
return [
    'settings' => [
        'displayErrorDetails' => $parameters['Application']['display_error_details'], // set to false in production
        'addContentLengthHeader' => $parameters['Application']['add_content_length_header'], // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' =>true,
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . $parameters['Application']['views']['path'],
        ],

        // Monolog settings
        'logger' => [
            'name' => $parameters['Application']['logs']['name'],
            'path' => __DIR__ . $parameters['Application']['logs']['path'],
            'level' => strtoupper((new ReflectionClass("\Monolog\Logger"))->getConstant($parameters['Application']['logs']['level']))
        ],
    ],
    "parameters"=>$parameters,
];
