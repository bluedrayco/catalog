<?php

// DIC configuration
$container = $app->getContainer();

 /**
 * Function that override the default Not Found handler
 * @param  mixed  $container container
 * @return mixed  $container container
 */
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $contentType = "application/json";
        if (array_key_exists("HTTP_ACCEPT", $request->getHeaders())) {
            $contentType = $request->getHeaders()["HTTP_ACCEPT"];
        }
        if (array_key_exists("HTTP_CONTENT_TYPE", $request->getHeaders())) {
            $contentType = $request->getHeaders()["HTTP_CONTENT_TYPE"];
        }
        return $container['response']
                        ->withStatus(404)
                        ->withHeader('Content-Type', $contentType)
                        ->write(json_encode(array('message' => 'Not found.')));
    };
};

/**
 * Function that override the default notFound handler
 * @param  mixed  $request  request
 * @param  mixed  $response response
 * @return mixed  $response response
 */
function notFound($request, $response) {
    $contentType = "application/json";
    if (array_key_exists("HTTP_ACCEPT", $request->getHeaders())) {
        $contentType = $request->getHeaders()["HTTP_ACCEPT"];
    }
    if (array_key_exists("HTTP_CONTENT_TYPE", $request->getHeaders())) {
        $contentType = $request->getHeaders()["HTTP_CONTENT_TYPE"];
    }
    return $response
                    ->withStatus(404)
                    ->withHeader('Content-Type', $contentType)
                    ->write(json_encode(array('message' => 'Not found.')));
}

/**
 * Function that override the default UserNotFound handler
 * @param  mixed  $request  request
 * @param  mixed  $response response
 * @return mixed  $response response
 */
function UserNotFound($request, $response) {
    $contentType = "application/json";
    if (array_key_exists("HTTP_ACCEPT", $request->getHeaders())) {
        $contentType = $request->getHeaders()["HTTP_ACCEPT"];
    }
    if (array_key_exists("HTTP_CONTENT_TYPE", $request->getHeaders())) {
        $contentType = $request->getHeaders()["HTTP_CONTENT_TYPE"];
    }
    return $response
                    ->withStatus(401)
                    ->withHeader('Content-Type', $contentType)
                    ->write(json_encode(array('message' => 'User Not Found.')));
}

/**
 * Function that override the default methodNotAllowed handler
 * @param  mixed  $request  request
 * @param  mixed  $response response
 * @return mixed  $response response
 */
function methodNotAllowed($request, &$response) {
    $contentType = "application/json";
    if (array_key_exists("HTTP_ACCEPT", $request->getHeaders())) {
        $contentType = $request->getHeaders()["HTTP_ACCEPT"];
    }
    if (array_key_exists("HTTP_CONTENT_TYPE", $request->getHeaders())) {
        $contentType = $request->getHeaders()["HTTP_CONTENT_TYPE"];
    }
    return $response
                    ->withStatus(405)
                    ->withHeader('Content-Type', $contentType)
                    ->write(json_encode(array('message' => 'Not method allowed.')));
}

;

// view renderer
$container['system.renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// -----------------------------------------------------------------------------
// Action factories
// -----------------------------------------------------------------------------

loadServices(__DIR__ . "/../Modules/");

function loadServices($path) {
    global $container;
    if (is_dir($path)) {
        if ($dh = opendir($path)) {
            while (($file = readdir($dh)) !== false) {
                if (!is_dir($path . $file)) {
                    $dir = substr($path, 0, -1);
                    $dir = substr($dir, strrpos($dir, "/") + 1);
                    if ($dir == 'Service' || $dir == 'Configuration') {
                        $pathResource = substr($path, strpos($path, "..") + 3);
                        $pathResource = strtolower(str_replace("/", ".", str_replace("Modules/", "", $pathResource)) . str_replace(".php", "", $file));
                        $pathResource = str_replace(".yml", "", $pathResource);

                        switch (pathinfo($path . $file, PATHINFO_EXTENSION)) {
                            case "php":
                                $container[$pathResource] = function ($c)use($path, $file) {
                                    $resource = str_replace(".php", "", "\\" . str_replace("/", "\\", substr($path, strpos($path, "..") + 3) . $file));
                                    return new $resource($c);
                                };
                                break;
                            case "yml":
                                $container[$pathResource] = function ($c)use($path, $file) {
                                    return \Spyc::YAMLLoad($path . $file);
                                };
                                break;
                        }
                    }
                }
                if (is_dir($path . $file) && $file != "." && $file != "..") {
                    loadServices($path . $file . "/");
                }
            }
            closedir($dh);
        }
    } else {
        $container->logger->error("\nIt isnt a valid route...");
        exit();
    }
}