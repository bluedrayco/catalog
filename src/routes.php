<?php

$app->get('/hola_mundo', function ($request, $response, $args) use ($container) {
    $this->logger->info("Slim-Skeleton '/' route");
    return $this['system.renderer']->render($response, 'index.phtml', $args);
});

$app->get('/api/documentation/{version}', function($request, $response, $args) use ($container) {
    return $this['system.renderer']->render($response, '../public/docs/index.html', ['host' => $_SERVER['HTTP_HOST'], 'version' => $args['version']]);
});

$app->get('/api/docs/{version}', function($request, $response, $args) use ($container) {
    $swagger = \Swagger\scan('../Modules/' . ucwords($args['version']));
    return $response
                    ->withStatus(200)
                    ->withHeader('Content-Type', 'application/json')
                    ->write($swagger);
});


/**
 * Function that given any url redirect to a valid controller function
 * @param  string $request   request
 * @param  string $response  response
 * @param  string $args      arguments
 * @return mixed  $object    valid controller function
 */
$app->any('/api/{version}/{controller}[/{resource}]', function($request, $response, $args) use ($container) {
    $class = "\\Modules\\" . ucwords($args['version']) . "\\Controller\\" . ucwords($args['controller']);
    if (!class_exists($class)) {
        switch ($request->getMethod()) {
            case 'GET':
                if (array_key_exists("resource", $args)) {
                        $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                        return $generic->getById($args['resource']);
                } else {
                    $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                    return $generic->getAll();
                }
                break;

            case 'DELETE':
                if (array_key_exists("resource", $args)) {
                        $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                        return $generic->delete($args['resource']);
                }
                break;

            case 'POST':
                if (!array_key_exists("resource", $args)) {
                    $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                    return $generic->create($request->getParsedBody());
                }
                break;

            case 'PUT':
                if (array_key_exists("resource", $args)) {
                    $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                    return $generic->update($args['resource'], $request->getParsedBody());
                }
                break;
        }

        return notFound($request, $response);
    }
    $object = new $class($this, $request, $response, $args);

    if (array_key_exists("resource", $args)) {
        if ($request->getMethod() == 'GET') {
            return $object->getById($args['resource']);
        }
    }

    if (!array_key_exists("resource", $args) && $request->getMethod() == 'GET') {
        return $object->getAll($request->getParsedBody());
    }

    if (!array_key_exists("resource", $args) && $request->getMethod() == 'POST') {
        return $object->create($request->getParsedBody());
    }

    if ($request->getMethod() == 'PUT') {
        return $object->update($args['resource'], $request->getParsedBody());
    }

    if (array_key_exists("resource", $args)) {
        if ($request->getMethod() == 'DELETE') {
            return $object->delete($args['resource']);
        }
    }

    $method = strtolower($request->getMethod()) . $args['resource'];
    if (!method_exists($object, $method) && !is_numeric($args['resource'])) {
        return methodNotAllowed($request, $response);
    }
    return $object->$method();
});
