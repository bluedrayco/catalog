<?php

use RedBeanPHP\Facade as R;
use Modules\Database;

if (array_key_exists("Database", $parameters)) {
    foreach ($parameters['Database']['connections'] as $name => $connection) {
        if ($name == 'default') {
            R::setup(Database::getStringConnection($connection), $connection['username'], $connection['password'], true);
        } else {
            R::addDatabase($name, Database::getStringConnection($connection), $connection['username'], $connection['password'], true);
        }
    }
    R::ext('xdispense', function( $type ) {
        return R::getRedBean()->dispense($type);
    });
}