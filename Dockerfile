FROM php:7.3-apache

ENV COMPOSER_VERSION 1.10.5
ENV APACHE_DOCUMENT_ROOT /var/www/microservice/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN apt-get update
RUN apt-get install -y git libpq-dev zip mariadb-client zlib1g-dev libzip-dev libpng-dev libicu-dev nano postgresql

RUN docker-php-ext-install gd 
RUN docker-php-ext-install bcmath 
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo_pgsql
RUN docker-php-ext-install zip
RUN docker-php-ext-install intl

RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
  && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
  && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
  && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION} && rm -rf /tmp/composer-setup.php

RUN apt-get -y autoremove

RUN composer global require hirak/prestissimo

RUN a2enmod rewrite

RUN mkdir -p /var/www/microservice

WORKDIR /var/www/microservice

COPY scripts/docker/start.sh /usr/local/bin/start.sh

RUN chmod 755 /usr/local/bin/start.sh

CMD ["/usr/local/bin/start.sh"]