<?php

/**
 * Base SDK
 */

namespace Modules;

/**
 * Base SDK
 *
 * Base class for the implementation of curl executions
 *
 * @package      Modules
 * @category     Base
 * @author       Roberto Leroy Monroy Ruiz <bluedrayco@gmail.com>
 */
class BaseSDK implements HTTPCodesInterface {

}
