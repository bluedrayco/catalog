<?php

/**
* Base Service
**/
namespace Modules;

/**
* Base Service
*
* Base class for the implementation of services
*
* @package      Modules
* @category     Base
* @author       Roberto Leroy Monroy Ruiz <bluedrayco@gmail.com>
*/
class BaseService implements HTTPCodesInterface{

    /**
     * construct from the base service base class
     * @param ContainerInterface $c dependency container from slim
     */
    public function __construct($c) {
        $this->container = $c;
    }

}
