<?php

/**
 * Base Lib
 */

namespace Modules;

use RedBeanPHP\Facade as R;

/**
 * Base Lib
 *
 * Base class for the implementation of libraries
 *
 * @package      Modules
 * @category     Base
 * @author       Roberto Leroy Monroy Ruiz <bluedrayco@gmail.com>
 */
class BaseLib implements HTTPCodesInterface {

    /**
     * this variable represents the dependency container
     * @var ContainerInterface
     */
    protected $container;
    /**
     * this variable represents the structure of the response
     * @var mixed
     */
    protected $response = array(
        'code' => BaseLib::HTTP_NOT_FOUND,
        'data' => null
    );

    /**
     * construct for the Base Library
     * @param ContainerInterface $container dependency container from slim
     */
    public function __construct($container) {
        $this->container = $container;
    }

    /**
     * Generic function to find a value for the id field
     * @param  string $table          name of the table
     * @param  string $referenceField reference field to order the query with
     * @return array  $id             valid id
     */
    public function getCurrentId($table,$idField ,$referenceField) {

        $id = R::getRow("select {$idField} from {$table} order by {$referenceField} desc limit 1;");
        $id = array_key_exists($idField, $id)?$id[$idField] + 1:1;
        return $id;
    }

    /**
     * Function that insert data in the table
     * @param  string     $table  table to do the insert
     * @param  string     $bean   bean to be inserted
     */
    public function createRegister($table, $bean) {
        $columns = R::getColumns($table);
        $beanAux = $bean;
        $bean = $bean->export();
        $query = "insert into {$table}(";
        $values = "(";
        $x = 0;
        foreach ($columns as $column => $value) {
            if (array_key_exists($column, $bean)) {
                $query.=$column;
                switch ($value) {
                    case'integer':
                    case'bigint':
                    case'numeric':
                        if ($bean[$column] === 0) {
                            $values.=0;
                        } elseif ($bean[$column] === null) {
                            $values.="null";
                        } else {
                            $values.=$bean[$column];
                        }

                        break;
                    case'boolean':
                        $values.=$bean[$column] == false ? "false" : "true";
                        breaK;
                    default:
                        if (strpos($value, "timestamp") !== false) {
                            $values.="'{$bean[$column]}'";
                        } else {
                            $text = $bean[$column];

                            $text = str_replace("'", "''", $text);
                            $values.="'{$text}'";
                        }

                        break;
                }
                $query.=",";
                $values.=",";
            }
        }
        $query = substr($query, 0, strlen($query) - 1);
        $values = substr($values, 0, strlen($values) - 1);
        $query.=")";
        $values.=")";
        $query.=" values " . $values;

        R::exec($query);
        return $beanAux;
    }

}
