<?php

/**
* HTTP codes CRUD
**/
namespace Modules;

/**
* HTTP codes CRUD
*
* Base interface for the implementation of HTTP codes
*
* @package      Modules
* @category     Base
* @author       Roberto Leroy Monroy Ruiz <bluedrayco@gmail.com>
*/
interface HTTPCodesInterface {

    /**
     * code for a successful request
     */
    const HTTP_OK = 200;
    /**
     * code for create a resource
     */
    const HTTP_CREATED = 201;
    /**
     * code used in asynchronous requests
     */
    const HTTP_ACCEPTED = 202;
    /**
     * code used when the response don't have body
     */
    const HTTP_NO_CONTENT = 204;
    /**
     * code used for a generic error like check parameters
     */
    const HTTP_BAD_REQUEST = 400;
    /**
     * code used when the client don't have an authorization to consume the resource
     */
    const HTTP_UNAUTHORIZED = 401;
    /**
     * code used when the client has an authorization to consume the api but he doesn't have enough permissions to consume the resource or action
     */
    const HTTP_FORBIDDEN = 403;
    /**
     * code used when the url doesn't exist
     */
    const HTTP_NOT_FOUND = 404;
    /**
     *code used when the url exists but the action doesn't exist
     */
    const HTTP_METHOD_NOT_ALLOWED = 405;

}
