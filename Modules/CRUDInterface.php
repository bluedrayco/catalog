<?php

/**
* Interface CRUD
**/
namespace Modules;

/**
* Interface CRUD
*
* Base interface for the implementation of CRUD
*
* @package      Modules
* @category     Base
* @author       Roberto Leroy Monroy Ruiz <bluedrayco@gmail.com>
*/
interface CRUDInterface{

    /**
     * get all the resources
     */
    public function getAll();
    /**
     * get a resource by its id
     * @param string $id identifier of the resource
     */
    public function getById($id);
    /**
     * create a new resource
     * @param mixed $data data for the new resource
     */
    public function create($data);
    /**
     * update a resource
     * @param string $id identifier from the resopurce to update
     * @param mixed $data data to modify from the resource
     */
    public function update($id,$data);
    /**
     * delete a resource
     * @param string $id identifier of the resource to delete
     */
    public function delete($id);
}