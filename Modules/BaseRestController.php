<?php

/**
 * Base controller
 */

namespace Modules;

use Slim\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RedBeanPHP\Facade as R;
use Modules\BaseLib as LBaseLib;

/**
 * Base Controller
 *
 * Base class for the implementation of controllers
 *
 * @package      Modules
 * @category     Base
 * @author       Roberto Leroy Monroy Ruiz <bluedrayco@gmail.com>
 */
class BaseRestController implements CRUDInterface, HTTPCodesInterface {

    /**
     * this is a dependency container from slim
     * @var ContainerInterface
     */
    protected $container;

    /**
     * this is the request from the client in slim
     * @var Psr\Http\Message\ServerRequestInterface
     */
    protected $request;

    /**
     * this is the response in slim
     * @var Psr\Http\Message\ResponseInterface
     */
    protected $response;

    /**
     * these are the parameters provided in the url
     * @var mixed
     */
    protected $args;

    /**
     * these are the parameters from the body request or the query parameters from the url
     * @var mixed
     */
    protected $params;

    /**
     * construct for create a controller storing its response, request and container.
     * @param \Slim\Container $c dependency container from the slim's kernel
     * @param \Psr\Http\Message\ServerRequestInterface $request object that represents the request from the rest service
     * @param \Psr\Http\Message\ResponseInterface $response object that represents the response from the rest service
     * @param mixed $args parameters received from the url
     */
    public function __construct(Container $c, Request $request, Response $response, $args) {
        $viewsPath = str_replace('Modules', "", substr(get_called_class(), 0, strrpos(get_called_class(), "\\")));
        $viewsPath = str_replace("\\", "/", __DIR__ . substr($viewsPath, 0, strrpos($viewsPath, "\\")) . "/View/");
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;
        $this->params = $this->getParams();
        $this->container = $c;
        $this->container['renderer'] = new \Slim\Views\PhpRenderer($viewsPath);
    }

    /**
     * Set the content of the response header
     * @param string $headerType header type
     * @return mixed   $this->response response
     */
    private function setContentHeader($headerType) {
        return $this->response->withHeader('Content-type', $headerType == null ? "application/json" : $headerType);
    }

    /**
     * Build the response of the microservice
     * @param  string  $data      response
     * @param  integer $code           response code
     * @param  string  $headerType     type of header
     * @return mixed   $this->response response
     */
    public function returnResponse($data, $code = null, $headerType = null) {
        $this->response = $this->setContentHeader($headerType);
        if ($data) {
            $this->response->getBody()->write(!$headerType ? json_encode($data) : $data);
        }
        $this->response = $this->response->withStatus(!$code ? $data['code'] : $code);
        return $this->response;
    }

    /**
     * Obtain the parameters of the request
     * @param  array  $urlParamsRequired params within the url
     * @return mixed  $params            params
     */
    private function getParams($urlParamsRequired = array()) {
        $params = array("body" => $this->request->getParsedBody(), "query" => $this->request->getQueryParams(), 'url' => $this->args);
        foreach ($urlParamsRequired as $clave) {
            if (!array_key_exists($clave, $params['url'])) {
                $params['url'][$clave] = null;
            }
        }
        return $params;
    }

    /**
     * Validate the params obtained within the url
     * @param  array   $params  params obtained
     * @param  array   $compare keys to compare with the params
     * @return boolean boolean  true or false depend on the match between params and compare
     */
    public function checkParameters($params, $compare) {
        foreach ($compare as $comp) {
            if (!array_key_exists($comp, $params)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generic create function for the microservice
     * @param  array $params               params obtained
     * @return mixed $this->returnResponse the new bean that has been created
     * @throws \Exception couldn't create the bean
     */
    public function create($params) {
        try {
            $baseLibrary = new LBaseLib($this->container);
            $table = $this->getResource();
            $configuration = $this->getEntityConfiguration($table);
            if (array_key_exists('database', $configuration)) {
                R::selectDatabase($configuration['database']);
            } else {
                R::selectDatabase('default');
            }
            $bean = R::xdispense($table);
            if (!$bean) {
                throw new \Exception("Failed to create entity {$table}");
            }
            if (array_key_exists('on_create', $configuration)) {
                if (array_key_exists('required', $configuration['on_create'])) {
                    if (!$this->checkParameters($params, $configuration['on_create']['required'])) {
                        throw new \Exception($this->checkParametersMessage($configuration['on_create']['required']), self::HTTP_BAD_REQUEST);
                    }
                }
                if (array_key_exists('control_fields', $configuration['on_create'])) {
                    foreach ($configuration['on_create']['control_fields'] as $field) {
                        $params[$field['name']] = $this->getFieldValue($field['type'], $field['value']);
                    }
                }
            }
            //generating id
            if (array_key_exists('id_generator', $configuration)) {
                $id = $baseLibrary->getCurrentId($table, $configuration['id_generator']['id_reference'], $configuration['id_generator']['order_by']);
                if (array_key_exists('id', $configuration)) {
                    $params[$configuration['id']] = $id;
                } else {
                    $params['id'] = $id;
                }
            }

            //import from $params to $bean
            $bean->import($params);

            //callbacks pre persist system level
            if (array_key_exists('database', $configuration)) {
                if (array_key_exists('lifecycle_callbacks', $this->container->get('parameters')['Database']['connections'][$configuration['database']])) {
                    if (array_key_exists('pre_persist', $this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks'])) {
                        foreach ($this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks']['pre_persist'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $bean = $result;
                            }
                        }
                    }
                }
                //callbacks pre_persist in entity.yml
                if (array_key_exists('lifecycle_callbacks', $configuration)) {
                    if (array_key_exists('pre_persist', $configuration['lifecycle_callbacks'])) {
                        foreach ($configuration['lifecycle_callbacks']['pre_persist'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $bean = $result;
                            }
                        }
                    }
                }
            }
            //persist the bean
            $bean = $baseLibrary->createRegister($table, $bean);
            $bean = $bean->fresh();
            //callbacks pre persist system level
            if (array_key_exists('database', $configuration)) {
                if (array_key_exists('lifecycle_callbacks', $this->container->get('parameters')['Database']['connections'][$configuration['database']])) {
                    if (array_key_exists('post_persist', $this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks'])) {
                        foreach ($this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks']['post_persist'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                        }
                    }
                }
                //callbacks post_persist in entity.yml
                if (array_key_exists('lifecycle_callbacks', $configuration)) {
                    if (array_key_exists('post_persist', $configuration['lifecycle_callbacks'])) {
                        foreach ($configuration['lifecycle_callbacks']['post_persist'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->container->logger->error($ex->getMessage());
            return $this->returnResponse(array('message' => $ex->getMessage()), self::HTTP_NOT_FOUND);
        }
        return $this->returnResponse($bean, self::HTTP_CREATED);
    }

    /**
     * get the value from the session or a field from the database
     * @param string $type
     * @param string $value data to obtain from the database
     * @return mixed value obtained from the database
     */
    protected function getFieldValue($type, $value) {
        $valueField = null;
        switch ($type) {
            case 'datetime':
            case 'time':
            case 'date':
                $valueField = new \DateTime($value);
                break;
            case 'session':
                $valueField = $this->container['user_claim'][$value];
                break;
            default:
                $valueField = $value;
                break;
        }
        return $valueField;
    }

    /**
     * create a message indicating the fields that we are need to store to the database
     * @param array $parameters string array that contains the required values
     * @return string string that indicates which fields are missing
     */
    protected function checkParametersMessage($parameters) {
        $message = "check parameters, they must be ";
        foreach ($parameters as $index => $value) {
            $message.= "'{$value}'";
            $message.= count($parameters) - 1 == $index ? "" : ",";
        }
        return $message;
    }

    /**
     * Evaluate if the action delete on the table is logical or physical
     * @param  string $table                 table to be configurated
     * @return mixed $this->array_key_exists configuration of the table
     */
    private function getEntityConfiguration($table) {
        $configuration = array();
        $configuration = array_key_exists($this->args['version'] . ".configuration.entity", $this->container->keys()) ? $this->container->get($this->args['version'] . ".configuration.entity") : array();
        $configuration = $this->container->get($this->args['version'] . ".configuration.entity");
        return array_key_exists($table, $configuration) ? $configuration[$table] : array();
    }

    /**
     * Generic delete function for the microservice
     * @param  integer $id                  id of the registry to be deleted
     * @return mixed  $this->returnResponse the bean that has been deleted
     * @throws \Exception couldn't delete the bean
     */
    public function delete($id) {
        try {
            $table = $this->getResource();
            $configuration = $this->getEntityConfiguration($table);
            if (array_key_exists('database', $configuration)) {
                R::selectDatabase($configuration['database']);
            } else {
                R::selectDatabase('default');
            }

            if (array_key_exists('id', $configuration)) {
                $identifier = $configuration['id'];
            } else {
                $identifier = 'id';
            }
            $bean = R::findOne($table, "{$identifier} = {$id}");
            if ($bean->$identifier == 0) {
                throw new \Exception("No resource {$table} founded with the id: {$id}");
            }
            //callbacks pre persist system level
            if (array_key_exists('database', $configuration)) {
                if (array_key_exists('lifecycle_callbacks', $this->container->get('parameters')['Database']['connections'][$configuration['database']])) {
                    if (array_key_exists('pre_remove', $this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks'])) {
                        foreach ($this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks']['pre_remove'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $bean = $result;
                            }
                        }
                    }
                }
                //callbacks pre_persist in entity.yml
                if (array_key_exists('lifecycle_callbacks', $configuration)) {
                    if (array_key_exists('pre_remove', $configuration['lifecycle_callbacks'])) {
                        foreach ($configuration['lifecycle_callbacks']['pre_remove'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $bean = $result;
                            }
                        }
                    }
                }
            }
            if (array_key_exists('on_delete', $configuration)) {
                if (array_key_exists('control_fields', $configuration['on_delete'])) {
                    foreach ($configuration['on_delete']['control_fields'] as $field) {
                        $bean[$field['name']] = $this->getFieldValue($field['type'], $field['value']);
                    }
                    $bean->import($bean->export());
                }
                R::store($bean);
            } else {
                R::trash($bean);
            }

            //callbacks pre persist system level
            if (array_key_exists('database', $configuration)) {
                if (array_key_exists('lifecycle_callbacks', $this->container->get('parameters')['Database']['connections'][$configuration['database']])) {
                    if (array_key_exists('post_remove', $this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks'])) {
                        foreach ($this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks']['post_remove'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                        }
                    }
                }
                //callbacks post_persist in entity.yml
                if (array_key_exists('lifecycle_callbacks', $configuration)) {
                    if (array_key_exists('post_remove', $configuration['lifecycle_callbacks'])) {
                        foreach ($configuration['lifecycle_callbacks']['post_remove'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->container->logger->error($ex->getMessage());
            return $this->returnResponse(array('message' => $ex->getMessage()), self::HTTP_NOT_FOUND);
        } catch (Security $ex) {
            $this->container->logger->error($ex->getMessage());
            return $this->returnResponse(array('message' => $ex->getMessage()), self::HTTP_NOT_FOUND);
        }
        return $this->returnResponse(null, self::HTTP_NO_CONTENT);
    }

    /**
     * List all the content of the table
     * @return mixed  $this->listResource all content of the table
     */
    public function getAll() {
        $resource = $this->getResource();
        return $this->listResource($resource, $this->params['query']);
    }

    /**
     * Generic function to search on registry in the table
     * @param  integer $id                  id of the registry to be searched out
     * @return mixed  $this->listResource the bean that has been searched
     */
    public function getById($id) {
        $resource = $this->getResource();
        return $this->listResource($resource, $this->params['query'], $id);
    }

    /**
     * Generic update function for the microservice
     * @param  integer $id                  id of the registry to be updated
     * @param  integer $data                data to update
     * @return mixed  $this->returnResponse the bean that has been updated
     * @throws \Exception couldn't update the bean
     */
    public function update($id, $data) {
        try {
            $table = $this->getResource();
            $configuration = $this->getEntityConfiguration($table);
            if (array_key_exists('database', $configuration)) {
                R::selectDatabase($configuration['database']);
            } else {
                R::selectDatabase('default');
            }
            if (array_key_exists('id', $configuration)) {
                $identifier = $configuration['id'];
            } else {
                $identifier = 'id';
            }
            $bean = R::findOne($table, "{$identifier} = {$id}");
            if ($bean->$identifier == 0) {
                throw new \Exception("No resource {$table} founded with the id: {$id}");
            }
            if (array_key_exists('on_update', $configuration)) {
                if (array_key_exists('required', $configuration['on_update'])) {
                    if (!$this->checkParameters($data, $configuration['on_update']['required'])) {
                        throw new \Exception($this->checkParametersMessage($configuration['on_update']['required']), self::HTTP_BAD_REQUEST);
                    }
                }
                if (array_key_exists('control_fields', $configuration['on_update'])) {
                    foreach ($configuration['on_update']['control_fields'] as $field) {
                        $bean[$field['name']] = $this->getFieldValue($field['type'], $field['value']);
                    }
                    $bean->import($bean->export());
                }
            }
            $bean->import($data);

            //callbacks pre persist system level
            if (array_key_exists('database', $configuration)) {
                if (array_key_exists('lifecycle_callbacks', $this->container->get('parameters')['Database']['connections'][$configuration['database']])) {
                    if (array_key_exists('pre_update', $this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks'])) {
                        foreach ($this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks']['pre_update'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $bean = $result;
                            }
                        }
                    }
                }
                //callbacks pre_persist in entity.yml
                if (array_key_exists('lifecycle_callbacks', $configuration)) {
                    if (array_key_exists('pre_update', $configuration['lifecycle_callbacks'])) {
                        foreach ($configuration['lifecycle_callbacks']['pre_update'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $bean = $result;
                            }
                        }
                    }
                }
            }

            R::store($bean);
            $bean = $bean->fresh();

            //callbacks pre persist system level
            if (array_key_exists('database', $configuration)) {
                if (array_key_exists('lifecycle_callbacks', $this->container->get('parameters')['Database']['connections'][$configuration['database']])) {
                    if (array_key_exists('post_update', $this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks'])) {
                        foreach ($this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks']['post_update'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                        }
                    }
                }
                //callbacks post_persist in entity.yml
                if (array_key_exists('lifecycle_callbacks', $configuration)) {
                    if (array_key_exists('post_update', $configuration['lifecycle_callbacks'])) {
                        foreach ($configuration['lifecycle_callbacks']['post_update'] as $callback) {
                            $result = call_user_func($callback, $this->container, $bean);
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->container->logger->error($ex->getMessage());
            return $this->returnResponse(array('message' => $ex->getMessage()), self::HTTP_NOT_FOUND);
        } catch (Security $ex) {
            $this->container->logger->error($ex->getMessage());
            return $this->returnResponse(array('message' => $ex->getMessage()), self::HTTP_NOT_FOUND);
        }
        return $this->returnResponse($bean, self::HTTP_OK);
    }

    /**
     * If the current microservice doesn't have a controller this function
     * will work with the generic function in this class and the controller
     * will be define by a parameter in the request
     * @return mixed $this->args['controller'] the name of the controller sended
     */
    private function getResource() {
        return $this->args['controller'];
    }

    /**
     * Function that is called by the functions getAll() and getById to search in
     * the table
     * @param  string     $table                name of the table to be searched
     * @param  array      $params               params obtained
     * @param  integer    $id                   id of the registry to be searched out
     * @return mixed      $this->returnResponse the bean that has been searched
     * @throws \Exception not found response
     */
    private function listResource($table, $params, $id = null) {
        try {
            $order = "";
            $dataOrder = "";
            if (array_key_exists('order', $params)) {
                $order = " order by ";
                $data = explode("|", $params['order']);
                if (count($data) < 2) {
                    throw new \Exception("Missing a parameter in the order field...");
                }
                $order = $order . $data[0] . " " . $data[1];
                $separationPoint = explode(".", $data[0]);
                $dataOrder = $separationPoint[0];
            }
            $columnsTable = R::getColumns($table);
            $configuration = $this->getEntityConfiguration($table);

            $filter = array_key_exists('filter', $params) ? $params['filter'] : "";
//            $relations = $this->getNameRelations($table,$columnsTable, $filter, $dataOrder);
            $relations = "";
//            $join = array_key_exists('filter', $params) ? $this->obtainJoins($table, $columnsTable, $params['filter'], $dataOrder) : '';
            $join = '';
            $tables = $relations == ' ' ? $table : "{$table} {$table}" . ($relations == '' ? '' : ",{$relations}");
            $fields = array_key_exists('fields', $params) ? $params['fields'] : $this->getTableFields($table, $columnsTable);
            $limit = array_key_exists('limit', $params) ? "limit " . $params['limit'] : "";
            $offset = array_key_exists('offset', $params) ? " offset " . $params['offset'] : '';
            $order = "";
            if (array_key_exists('order', $params)) {
                $order = " order by ";
                $data = explode("|", $params['order']);
                if (count($data) < 2) {
                    throw new \Exception("Missing a parameter in the order field...");
                }
                $order = $order . $data[0] . " " . $data[1];
            }

            $where = $join != '' || $filter != "" ? ' WHERE ' : "";
            $where.=$join == '' ? '' : $join;
            if ($filter) {
                $filter = $join != '' ? ' AND ' . $filter : $filter;
            }

            $query = "SELECT {$fields} FROM {$tables} {$where} {$filter}  {$order} {$limit} {$offset}";

            $query = str_replace("'||", "'%", $query);
            $query = str_replace("||'", "%'", $query);
        //    print_r($query);
        //    print_r($params);
        //    exit();
            if ($id == null) {
                $records = R::getAll($query);
            } else {
                if (array_key_exists('id', $configuration)) {
                    $identifier = $configuration['id'];
                } else {
                    $identifier = 'id';
                }
                $records = R::findOne($table, "{$identifier} = ?", [$id]);
            }
            //callbacks pre persist system level
            if (array_key_exists('database', $configuration)) {
                if (array_key_exists('lifecycle_callbacks', $this->container->get('parameters')['Database']['connections'][$configuration['database']])) {
                    if (array_key_exists('post_load', $this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks'])) {
                        foreach ($this->container->get('parameters')['Database']['connections'][$configuration['database']]['lifecycle_callbacks']['post_load'] as $callback) {
                            $result = call_user_func($callback, $this->container, $records);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $records = $result;
                            }
                        }
                    }
                }
                //callbacks pre_persist in entity.yml
                if (array_key_exists('lifecycle_callbacks', $configuration)) {
                    if (array_key_exists('post_load', $configuration['lifecycle_callbacks'])) {
                        foreach ($configuration['lifecycle_callbacks']['post_load'] as $callback) {
                            $result = call_user_func($callback, $this->container, $records);
                            if ($result instanceof RedBeanPHP\OODBBean) {
                                $records = $result;
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->container->logger->error($e->getMessage());
            return $this->returnResponse(array("message" => $e->getMessage()), self::HTTP_NOT_FOUND);
        }
        return $this->returnResponse(array_key_exists('count', $params) ? array('count' => count($records)) : $records, $records == NULL ? self::HTTP_NO_CONTENT : self::HTTP_OK);
    }

    /**
     * Function that find the relation of the table and create
     * the alias for the query to the database
     * @param  string     $table       name of the table
     * @param  string     $columns     columns of the table
     * @param  string     $filter      filter to apply to the search
     * @param  string     $order       order to apply to the search
     * @return string     string       relations of the table
     */
    private function getNameRelations($table, $columns, $filter, $order) {
        $relations = "";
        $configuration = $this->getEntityConfiguration($table);
        if (array_key_exists('id', $configuration)) {
            $id = $configuration['id'];
        } else {
            $id = 'id';
        }
        foreach ($columns as $key => $value) {
            if (strpos($key, "_id") && $key != $id && ($this->existInString($key, $filter) || $this->existInString($order, $key))) {
                $key = str_replace("_id", "", $key);
                $relations.= "{$key} {$key},";
            }
        }
        return substr($relations, 0, strlen($relations) - 1);
    }

    /**
     * Function that find the fields of the table
     * @param  string     $table     name of the table
     * @param  string     $columns   filter to apply to the search
     * @return string     string     fields of the table
     */
    private function getTableFields($table, $columns) {
        $tmp = "";
        foreach ($columns as $key => $value) {
            $tmp.="{$table}.{$key},";
        }
        return substr($tmp, 0, strlen($tmp) - 1);
    }

    /**
     * Function that evaluate if there is a extra relation
     * in the query and created
     * @param  string     $table     name of the table
     * @param  string     $columns   columns of the table
     * @param  string     $filter    filter to apply to the search
     * @param  string     $order     order to apply to the search
     * @return string     string     joins of the table
     */
    private function obtainJoins($table, $columns, $filter, $order) {
        $relations = "";
        $x = 0;
        $configuration = $this->getEntityConfiguration($table);
        if (array_key_exists('id', $configuration)) {
            $id = $configuration['id'];
        } else {
            $id = 'id';
        }

        foreach ($columns as $key => $value) {
            if (($this->existInString($key, $filter) || $this->existInString($order, $key)) && $key != $id) {
                if ($x == 0 && strpos($key, "_id")) {
                    $relations.="(";
                }
                if ($x != 0) {
                    $relations.=" and ";
                }
                if (strpos($key, "_id")) {
                    $relatedTable = str_replace("_id", "", $key);
                    $configurationRelatedTable = $this->getEntityConfiguration($table);
                    if (array_key_exists('id', $configurationRelatedTable)) {
                        $idRelatedTable = $configurationRelatedTable['id'];
                    } else {
                        $idRelatedTable = 'id';
                    }
                    $relations.="{$table}.{$key}={$relatedTable}.{$idRelatedTable}";
                    $x++;
                }
            }
        }
        if ($x != 0) {
            $relations.=")";
        }
        return $relations;
    }

    /**
     * Function that evaluate if there is word in a string
     * @param  string     $key      key to search
     * @param  string     $cadena   strin to search in
     * @return string     string    joins of the table
     */
    private function existInString($key, $cadena) {
        $key = $key == '' ? 'NonExist' : $key;
        return strpos($cadena, $key) !== false;
    }

}
