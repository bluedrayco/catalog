<?php
namespace Modules\Callbacks;

final class Catalog{
    static function primerCallback($container,$bean){
        echo "entre al primer callback pre del sistema...\n";
//        print_r($container);
        print_r($bean->export());
        $bean->nombre="roberto leroy..42342322sss2.";
        return $bean;
    }
    static function segundoCallback($container,$bean){
        echo "entre al segundo callback pre del sistema...\n";
        return $bean;
    }

    static function callbackPostUno($container,$bean){
        echo "entre a primer callback post del sistema...\n";
    }
    static function callbackPostDos($container,$bean){
        echo "entre a segundo callback post del sistema...\n";
    }
    static function callbackPostTres($container,$bean){
        echo "entre a tercer callback post del sistema...\n";
    }

    static function preRemove1($container,$bean){
        echo "aaaaaaaaaaaaa";
    }
    
    static function postRemove1($container,$bean){
        echo "bbbbbbbbbbbbbbbbbb";
    }
    
    static function postRemove2($container,$bean){
        echo "cccccccccccccccccccc";
    }
    
    static function preUpdate1($container,$bean){
        echo "aaaaaaaaaaaaaaaa";
    }
    
    static function preUpdate2($container,$bean){
        echo "bbbbbbbbbbbbbbbb";
    }
    static function postUpdate1($container,$bean){
        echo "cccccccccccccccc";
    }
    
    static function postLoad1($container,$bean){
        echo "aaaaaaaaaaaaaaaa";
    }
    static function postLoad2($container,$bean){
        echo "bbbbbbbbbbbbbbbb";
    }
}