<?php

/**
* Class database
**/
namespace Modules;

/**
* Class database
*
* Class for the connection to the database
*
* @package      Modules
* @category     Base
* @author       Roberto Leroy Monroy Ruiz <bluedrayco@gmail.com>
*/
class Database{

     /**
     * Generic function to connect to a database
     * @param  mixed  $params      params obtained
     * @return mixed  $connection  valid connection
     */
    public static function getStringConnection($params){
        $user  = $params['username'];
        $password = $params['password'];
        $connection = '';

        switch($params['driver']){
            case 'pgsql':
                $connection = 'pgsql:dbname='.$params['database'].';host='.$params['host'];
                break;

            case 'sqlite':
                $connection = 'sqlite:'.$params['database'];
                $user  = null;
                $password = null;
                break;

            case 'sqlite_memory':
                $connection = 'sqlite::memory';
                $user  = null;
                $password = null;
                break;

            case 'mysql':
                $connection = 'mysql:host='.$params['host'].';dbname='.$params['database'].($params['charset']!=''?(';charset='.$params['charset']):'').($params['port']!=''?';port='.$params['port']:'');
                break;

            case 'firebird':
                $connection = 'firebird:dbname='.$params['host'].':'.$params['database'];
                break;

            case 'informix':
                $connection = 'informix:'.$params['host'].'='.$params['database'];
                break;

            case 'oracle':
                $connection = 'OCI:dbname='.$params['database'].($params['charset']!=''?(';charset='.$params['charset']):'');
                break;

            case 'odbc':
                $connection = 'odbc:Driver={SQL Native Client};Server='.$params['host'].';Database='.$params['database'].'; Uid='.$params['username'].';Pwd='.$params['password'].';';
                $user  = null;
                $password = null;
                break;

            case 'dblib':
                $connection = 'dblib:host='.$params['host'].':'.$params['port'].';dbname='.$params['database'];
                break;

            case 'db2':
                $connection = 'ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE='.$params['database'].'; HOSTNAME='.$params['host'].($params['port']!=''?';PORT='.$params['port']:'').';PROTOCOL=TCPIP;';
                break;

            default:
                throw new \Exception('Database unknown...');
        }

        return $connection;
    }
}