<?php

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

$parameters = \Spyc::YAMLLoad(__DIR__ . '/../configuration/application.yml');

use Common\Colors;
use RedBeanPHP\Facade as R;
use Modules\Database;

$colors = new Colors();
$app = new Silly\Application();


function synchronizeWithMysql($database, $output, $colors):array
{
    $beans =R::getAll("
        SELECT table_name,column_name 
        FROM information_schema.key_column_usage
        WHERE table_schema = 'esans3' and constraint_name='PRIMARY' order by table_name asc
    ");
    foreach ($beans as $bean) {
        $output->writeln($colors->getColoredString('Entity founded: ', 'white', 'black') . $colors->getColoredString($bean['table_name'], 'green', 'black'));
        $parameters_entity[$bean['table_name']]['id'] = $bean['column_name'];
        $parameters_entity[$bean['table_name']]['database'] = $database;
    }
    return $parameters_entity;
}

function synchronizeWithPostgresql($database, $output, $colors):array
{
    $beans = R::getAll("
    SELECT  t.table_catalog,
        t.table_schema,
        t.table_name,
        kcu.constraint_name,
        kcu.column_name,
        kcu.ordinal_position
    FROM    INFORMATION_SCHEMA.TABLES t
                LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
                        ON tc.table_catalog = t.table_catalog
                        AND tc.table_schema = t.table_schema
                        AND tc.table_name = t.table_name
                        AND tc.constraint_type = 'PRIMARY KEY'
                LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
                        ON kcu.table_catalog = tc.table_catalog
                        AND kcu.table_schema = tc.table_schema
                        AND kcu.table_name = tc.table_name
                        AND kcu.constraint_name = tc.constraint_name
    WHERE   t.table_schema NOT IN ('pg_catalog', 'information_schema')
    ORDER BY t.table_catalog,
                t.table_schema,
                t.table_name,
                kcu.constraint_name,
                kcu.ordinal_position; 
    ");
    foreach ($beans as $bean) {
        if ($bean['column_name']) {
            if (!$bean['ordinal_position' > 1]) {
                $output->writeln($colors->getColoredString('Entity founded: ', 'white', 'black') . $colors->getColoredString($bean['table_name'], 'green', 'black'));
                $parameters_entity[$bean['table_name']]['id'] = $bean['column_name'];
                $parameters_entity[$bean['table_name']]['database'] = $database;
            } else {
                unset($parameters_entity[$bean['table_name']]);
            }
        }
    }
    return $parameters_entity;
}

function createDirectoryIfNotExists($directoryName)
{
    if (file_exists($directoryName)) {
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0700, true);
        }
    } else {
        mkdir($directoryName, 0777, true);
    }
}

function createFileIfNotExists($fileName)
{
    if (!file_exists($fileName)) {
        $file = fopen($fileName . ".sql", "w");
        fclose($file);
    }
}

function executeSqlStatements($statements)
{
    $sqlOrder = array(
        0 => 'NewSchemas',
        1 => 'ModifySchemas',
        2 => 'Views',
        3 => 'StoreProcedures',
        4 => 'Triggers',
        5 => 'InsertUpdateDelete'
    );
    R::begin();
    foreach ($sqlOrder as $order) {
        foreach ($statements[$order] as $index => $sql) {
            try {
                R::exec($sql);
                $statements[$order][$index] = array('sql' => $sql, 'result' => true, 'message' => 'ok');
            } catch (\Exception $ex) {
                $statements[$order][$index] = array('sql' => $sql, 'result' => false, 'message' => $ex->getMessage());
            }
        }
    }
    R::rollback();
    return $statements;
}

function executeSqlStatementsAtomic($tag_name, $statements, $output, $color)
{
    $sqlOrder = array(
        0 => 'NewSchemas',
        1 => 'ModifySchemas',
        2 => 'Views',
        3 => 'StoreProcedures',
        4 => 'Triggers',
        5 => 'InsertUpdateDelete'
    );
    try {
        foreach ($sqlOrder as $order) {
            foreach ($statements[$order] as $index => $sql) {
                R::exec($sql);
                $output->writeln();
            }
        }
    } catch (\Exception $ex) {
        throw new \Exception("tag_name: {$tag_name}, file: {$order}, error:" . $ex->getMessage());
    }
    return $statements;
}

function extractSqlStatementsMigrations($database, $version, $tag)
{
    $statements = array(
        'InsertUpdateDelete' => [],
        'ModifySchemas' => [],
        'NewSchemas' => [],
        'StoreProcedures' => [],
        'Triggers' => [],
        'Views' => []
    );
    foreach ($statements as $section => $value) {
        $filename = __DIR__ . "/migrations/database/{$database}/{$version}/{$tag}/{$section}.sql";
        if (file_exists($filename)) {
            $statements[$section] = SqlFormatter::splitQuery(SqlFormatter::compress(file_get_contents($filename)));
        }
    }
    return $statements;
}

function getStructureMigrations($database, $api_version, $delete_prefix_migration = true)
{
    $directoryDatabaseNames = array();
    if ($database == 'all') {
        $databaseNamePath = __DIR__ . '/migrations/database/*';
        $directoryDatabaseNames = glob($databaseNamePath, GLOB_ONLYDIR);
    } else {
        $databaseNamePath = __DIR__ . '/migrations/database/' . $database;
        if (file_exists($databaseNamePath) and is_dir($databaseNamePath)) {
            $directoryDatabaseNames = array($databaseNamePath);
        } else {
            $directoryDatabaseNames = array();
        }
    }
    $migrations = array();
    foreach ($directoryDatabaseNames as $ddn) {
        if ($api_version == 'all') {
            $migrationVersionPath = $ddn . '/*';
            $migrationVersionsPath = glob($migrationVersionPath, GLOB_ONLYDIR);
        } else {
            $migrationVersionPath = $ddn . '/' . ucwords($api_version);
            if (file_exists($migrationVersionPath) and is_dir($migrationVersionPath)) {
                $migrationVersionsPath = array($migrationVersionPath);
            } else {
                $migrationVersionsPath = array();
            }
        }
        $database = str_replace(__DIR__ . '/migrations/database/', '', $ddn);
        foreach ($migrationVersionsPath as $mvp) {
            $version = str_replace(__DIR__ . '/migrations/database/' . $database . '/', '', $mvp);
            $migrationNamePath = $mvp . '/*';
            $migrationNamesPath = glob($migrationNamePath, GLOB_ONLYDIR);
            foreach ($migrationNamesPath as $mnp) {
                $migrations[$database][$version][] = $delete_prefix_migration == true ? explode("__", $mnp)[1] : str_replace(__DIR__ . '/migrations/database/' . $database . '/' . ucwords($api_version) . '/', '', $mnp);
            }
        }
    }
    return $migrations;
}

$app->command('database:schema:entity:synchronize [connection] [api_version]', function ($connection, $api_version, OutputInterface $output) use ($parameters, $colors) {
    try {
        $text = "Connection: {" . $connection . "}  version: {" . $api_version . "}";
        $output->writeln($colors->getColoredString($text, 'light_green', 'black'));
        $filename_entity = __DIR__ . '/../Modules/' . ucwords($api_version) . '/Configuration/' . $parameters['Application']['entity_configuration_name_file'] . '.yml';
        if (!file_exists($filename_entity)) {
            throw new \Exception("The entity file wasn't founded, the version it's wrong...");
        }
        $parameters_entity = \Spyc::YAMLLoad($filename_entity);
        if (!array_key_exists($connection, $parameters['Database']['connections'])) {
            throw new \Exception("The configuration's database was not founded, the database is wrong please see the configuration.yml from the microservice...");
        }
        R::setup(Database::getStringConnection($parameters['Database']['connections'][$connection]), $parameters['Database']['connections'][$connection]['username'], $parameters['Database']['connections'][$connection]['password'], true);
        switch ($parameters['Database']['connections'][$connection]['driver']) {
            case 'pgsql':
                $parameters_entity=synchronizeWithPostgresql($connection, $output, $colors);
            break;
            case 'mysql':
                $parameters_entity=synchronizeWithMysql($connection, $output, $colors);
            break;
            default:
                throw new \Exception('The driver is not suppported');
            break;
        }
        file_put_contents($filename_entity, Spyc::YAMLDump($parameters_entity));
        $output->writeln($colors->getColoredString("\nThe entity file was synchronized with the database...", 'green'));
    } catch (\Exception $ex) {
        $output->writeln($ex->getMessage());
        exit();
    }
})->descriptions('Synchronize each entity from the specified database', [
    'connection' => 'connection to obtain the information to connect from the application.yml config file',
    'api_version' => 'the version where is the entity to update'
])->defaults([
    'connection' => 'default',
    'api_version' => 'v1'
]);

$app->command('database:migration:install connection api_version begin_tag [final_tag]', function ($connection, $api_version, $begin_tag, $final_tag, InputInterface $input, OutputInterface $output) use ($parameters, $colors) {
    try {
        $text = "Connection: {" . $connection . "}  version: {" . $api_version . "} begin_tag: {" . $begin_tag . "} final_tag: {" . $final_tag . "}";
        $output->writeln($colors->getColoredString($text, 'light_green', 'black') . "\n");
        $migrations = getStructureMigrations($connection, $api_version, false);
        $sqlQuerys = array();
        $flag = false;
        foreach ($migrations[$connection] as $version => $tags) {
            foreach ($tags as $tag) {
                $tagTmp = explode("__", $tag)[1];
                if ($tagTmp == $begin_tag || $begin_tag == 'begin') {
                    $flag = true;
                }
                if ($flag == true) {
                    $sqlQuerys[$tag] = extractSqlStatementsMigrations($connection, $version, $tag);
                }
                if ($tagTmp == $final_tag) {
                    break;
                }
            }
        }

        R::setup(Database::getStringConnection($parameters['Database']['connections'][$connection]), $parameters['Database']['connections'][$connection]['username'], $parameters['Database']['connections'][$connection]['password'], true);
        R::begin();
        try {
            foreach ($sqlQuerys as $tag => $value) {
                executeSqlStatementsAtomic($tag, $sqlQuerys[$tag], $output, $colors);
            }
            R::commit();
        } catch (\Exception $ex) {
            $output->writeln($colors->getColoredString("Execution error: " . $ex->getMessage(), 'red', 'black'));
            $output->writeln($colors->getColoredString("\nThe transaction was aborted...", 'red', 'black'));
            R::rollback();
        }
    } catch (\Exception $ex) {
        $output->writeln($ex->getMessage());
        exit();
    }
})->descriptions('Install a migration specifying the first tag and the last tag, if you dont specify the tags they will be from the fist tag to last tag, this operation is atomic, if it provoques a problem it will do rollback in the database.', [
    'begin_tag' => 'this tag will be the first tag ',
    'final_tag' => 'the last  migration tag name to install',
    'connection' => 'connection we need to show the migration tag names',
    'api_version' => 'the version we need to show the migration tag names'
])->defaults([
    'final_tag' => 'latest'
]);

$app->command('database:migration:check connection api_version', function ($connection, $api_version, InputInterface $input, OutputInterface $output) use ($parameters, $colors) {
    try {
        $text = "Connection: {" . $connection . "}  version: {" . $api_version . "}";
        $output->writeln($colors->getColoredString($text, 'light_green', 'black') . "\n");
        $migrations = getStructureMigrations($connection, $api_version, false);
        $sqlQuerys = array();
        foreach ($migrations[$connection] as $version => $tags) {
            foreach ($tags as $tag) {
                $sqlQuerys[$tag] = extractSqlStatementsMigrations($connection, $version, $tag);
            }
        }
        $statusQuerys = array();
        R::setup(Database::getStringConnection($parameters['Database']['connections'][$connection]), $parameters['Database']['connections'][$connection]['username'], $parameters['Database']['connections'][$connection]['password'], true);
        foreach ($sqlQuerys as $tag => $value) {
            $statusQuerys[$tag] = executeSqlStatements($sqlQuerys[$tag]);
        }
        foreach ($statusQuerys as $tag => $content) {
            $output->writeln($colors->getColoredString("\n\n" . $tag, 'white', 'black') . "\n");
            foreach ($statusQuerys[$tag] as $type => $sqls) {
                if (!empty($sqls)) {
                    $output->writeln($colors->getColoredString("\t\t" . $type, 'black', 'yellow') . "\n");
                    foreach ($sqls as $sql) {
                        if ($sql['result']) {
                            $output->write($colors->getColoredString("\t\t\t" . $sql['sql'], 'green', 'black'));
                        } else {
                            $output->write($colors->getColoredString("\t\t\t" . $sql['sql'], 'light_red', 'black'));
                        }
                        $output->writeln($colors->getColoredString(" -> " . $sql['message'], 'light_blue', 'black'));
                    }
                    $output->writeln("");
                }
            }
        }
    } catch (\Exception $ex) {
        $output->writeln($ex->getMessage());
        exit();
    }
})->descriptions('Check the consistence of each sql statements and show the problems in a specific sql and file', [
    'connection' => 'connection from the application.yml where we need to check the consistence.',
    'api_version' => 'the version we need to check the consistence',
]);

$app->command('database:migration:list [--api_version=] [--connection=]', function ($api_version, $connection, InputInterface $input, OutputInterface $output) use ($parameters, $colors) {
    try {
        $text = "Connection: {" . $connection . "}  version: {" . $api_version . "}";
        $output->writeln($colors->getColoredString($text, 'light_green', 'black') . "\n");
        $migrations = getStructureMigrations($connection, $api_version);
        foreach ($migrations as $dkey => $dvalue) {
            $output->writeln($colors->getColoredString($dkey, 'purple', 'black') . "\n");
            foreach ($dvalue as $vkey => $vvalue) {
                $output->writeln($colors->getColoredString("\t" . $vkey, 'white', 'black'));
                foreach ($vvalue as $mkey) {
                    $output->writeln($colors->getColoredString("\t\t" . $mkey, 'green', 'black'));
                }
            }
        }
    } catch (\Exception $ex) {
        $output->writeln($ex->getMessage());
        exit();
    }
})->descriptions('List the migration tag names specifying the database or api_version', [
    '--connection' => 'connection name we need to show the migration tag names',
    '--api_version' => 'the version we need to show the migration tag names'
])->defaults([
    'connection' => 'all',
    'api_version' => 'all'
]);

$app->command('database:migration:create  migration_name [connection] [api_version]', function ($migration_name, $connection, $api_version, InputInterface $input, OutputInterface $output) use ($parameters, $colors) {
    try {
        $text = "Connection: {" . $connection . "}  version: {" . $api_version . "}  migration name: {" . $migration_name . "}";
        $output->writeln($colors->getColoredString($text, 'light_green', 'black'));
        $helper = $this->getHelperSet()->get('question');

        $question = new ConfirmationQuestion('The parameters are corrects?(yes|no) ', false);

        if (!$helper->ask($input, $output, $question)) {
            $output->writeln($colors->getColoredString("The process was interrupted by the user", 'red', 'black'));
        }
        $migrationDatabasePath = __DIR__ . '/migrations/database/' . $connection;
        $migrationNamePath = __DIR__ . '/migrations/database/' . $connection . "/" . ucwords($api_version);
        $migrationNameDirectory = __DIR__ . '/migrations/database/' . $connection . "/" . ucwords($api_version) . "/" . time() . "__" . $migration_name;
        $sectionsNames = array(
            'ModifySchemas',
            'NewSchemas',
            'StoreProcedures',
            'Triggers',
            'Views',
            'InsertUpdateDelete'
        );
        $exp = $migrationNamePath . "/*" . $migration_name;
        $directoryMigrationNames = glob($exp, GLOB_ONLYDIR);
        if (count($directoryMigrationNames) > 0) {
            $output->writeln($colors->getColoredString("This migration name was founded, the path is: " . $directoryMigrationNames[0], 'red', 'black'));
            exit();
        }
        createDirectoryIfNotExists($migrationDatabasePath);
        createDirectoryIfNotExists($migrationNameDirectory);
            foreach ($sectionsNames as $sectionsName) {
                createFileIfNotExists($migrationNameDirectory . "/" . $sectionsName);
                $output->writeln($colors->getColoredString($migrationNameDirectory . "/" . $sectionsName . ".sql\t\tfile was created...", 'white', 'black'));
            }
        $output->writeln("\n\nThe migration structure was created, if you don't use any sql file you can delete it...");
    } catch (\Exception $ex) {
        $output->writeln($ex->getMessage());
    }
})->descriptions('Create a new migration', [
    'connection' => 'connection to the database to obtain the information to connect from the application.yml config file',
    'api_version' => 'The version where is the entity to update'
])->defaults([
    'connection' => 'default',
    'api_version' => 'v1'
]);


$app->run();
